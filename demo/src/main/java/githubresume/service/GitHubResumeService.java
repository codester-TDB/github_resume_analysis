package githubresume.service;

import githubresume.model.GithubRepository;
import githubresume.model.GithubUserResume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class GitHubResumeService {
    // We are using all default settings for our Rest communications in this service.
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Value("${github.restapi.url}")
    private String GITHUB_API_URL;

    public GithubUserResume execute(String userName) {
        VanillaRestServiceImpl<GithubUserResume.Builder> githubUserResumeRestService =
                new VanillaRestServiceImpl(restTemplateBuilder);
        GithubUserResume.Builder ghResumeBuilder = githubUserResumeRestService.getDetails(
                GITHUB_API_URL + userName, HttpMethod.GET, null, GithubUserResume.Builder.class);
        VanillaRestServiceImpl<GithubRepository[]> githubRepositoryService =
                new VanillaRestServiceImpl(restTemplateBuilder);
        GithubRepository[] githubRepositories = githubRepositoryService.getDetails(
                ghResumeBuilder.getReposURL(), HttpMethod.GET, null, GithubRepository[].class);
        fillUpTheRepoDetailsAndLangAggr(ghResumeBuilder, githubRepositories);
        return ghResumeBuilder.build();
    }

    public void fillUpTheRepoDetailsAndLangAggr(
            GithubUserResume.Builder ghResumeBuilder, GithubRepository[] githubRepositories) {
        ghResumeBuilder.setOwnedRepos(Arrays.asList(githubRepositories));

        long sum = ghResumeBuilder.getOwnedRepos().stream().mapToLong((e) -> e.getSize()).sum();
        Iterator<GithubRepository> it = ghResumeBuilder.getOwnedRepos().listIterator();
        Map<String, Long> totalSizeOfEachLang = new HashMap();

        while(it.hasNext()) {
            GithubRepository repo = it.next();
            if(totalSizeOfEachLang.containsKey(repo.getLanguage())) {
                totalSizeOfEachLang.put(
                        repo.getLanguage(), totalSizeOfEachLang.get(repo.getLanguage())
                                + repo.getSize());
            } else {
                totalSizeOfEachLang.put(
                        repo.getLanguage(), repo.getSize());
            }
        }
        totalSizeOfEachLang.keySet().stream().forEach(
                (e) -> totalSizeOfEachLang.put(
                        e, totalSizeOfEachLang.get(e).longValue() * 100 /sum));
        ghResumeBuilder.setLanguageAggr(totalSizeOfEachLang);
    }
}
