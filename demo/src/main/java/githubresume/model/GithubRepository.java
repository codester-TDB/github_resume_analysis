package githubresume.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class GithubRepository implements Serializable {
    private final String name;
    @JsonProperty("html_url")
    private final String url;
    private final String description;
    private final String language;
    private final long size;

    public String getLanguage() {
        return language;
    }

    public long getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public GithubRepository(String name, String url, String description, String language, long size) {
        this.name = name;
        this.url = url;
        this.description = description;
        this.language = language;
        this.size = size;
    }

    @Override
    public String toString() {
        return "GithubRepository{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", language=" + language +
                ", size=" + size +
                '}';
    }
}