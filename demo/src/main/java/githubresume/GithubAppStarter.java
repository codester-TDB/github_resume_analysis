package githubresume;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubAppStarter {
    public static void main(String[] args) {
        SpringApplication.run(GithubAppStarter.class, args);
        System.out.println("#######################");
        System.out.println("##### Initialized! ####");
        System.out.println("#######################");
    }
}
