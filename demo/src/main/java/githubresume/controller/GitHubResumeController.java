package githubresume.controller;

import githubresume.model.GithubUserResume;
import githubresume.model.UserNameInputForm;
import githubresume.service.GitHubResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class GitHubResumeController {

    @Autowired
    GitHubResumeService gitHubResumeService;

    /*
    Default controller, helps in landing to search/home page for this application.
     */
    @GetMapping(value = {"/search", "/"})
    public String searchFoGithubUser(Model model) {
        model.addAttribute("userNameInputForm", new UserNameInputForm());
        return "search-user";
    }

    /*
    Search request handler, that hits https://api.github.com and parses the
    necessary information for you.
     */
    @GetMapping("/githubuser")
    public String getGitHubResume(
            @Valid UserNameInputForm userNameInputForm, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors())
            return "search-user";
        GithubUserResume githubUserResume = gitHubResumeService.execute(
                userNameInputForm.getGithubUserName());
        model.addAttribute("githubUserResume", githubUserResume);
        return "user_resume";
    }

}
