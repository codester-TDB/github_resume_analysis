package githubresume.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class GithubUserResume implements Serializable {
    @JsonProperty
    private final String ghUserName;

    @JsonProperty
    private final String ghPageURL;

    @JsonProperty
    private final String blogURL;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final Iterable<GithubRepository> ownedRepos;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final Map<String, Long> languageAggr;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Builder implements Serializable{
        @JsonProperty("login")
        private String ghUserName;
        @JsonProperty("html_url")
        private String ghPageURL;
        @JsonProperty("blog")
        private String blogURL;
        @JsonProperty("repos_url")
        private String reposURL;

        private List<GithubRepository> ownedRepos;
        private Map<String, Long> languageAggr;

        @JsonCreator
        public Builder(@JsonProperty("login") String ghUserName) {
            this.ghUserName = ghUserName;
            this.ghPageURL = ghUserName;
        }

        public GithubUserResume build() {
            return new GithubUserResume(this);
        }

        public String getGhUserName() {
            return ghUserName;
        }

        public Builder setGhUserName(String ghUserName) {
            this.ghUserName = ghUserName;
            return this;
        }

        public String getGhPageURL() {
            return ghPageURL;
        }

        public Builder setGhPageURL(String ghPageURL) {
            this.ghPageURL = ghPageURL;
            return this;
        }

        public String getBlogURL() {
            return blogURL;
        }

        public Builder setBlogURL(String blogURL) {
            this.blogURL = blogURL;
            return this;
        }

        public List<GithubRepository> getOwnedRepos() {
            return ownedRepos;
        }

        public Builder setOwnedRepos(List<GithubRepository> ownedRepos) {
            this.ownedRepos = ownedRepos;
            return this;
        }

        public Map<String, Long> getLanguageAggr() {
            return languageAggr;
        }

        public Builder setLanguageAggr(Map<String, Long> languageAggr) {
            this.languageAggr = languageAggr;
            return this;
        }


        public String getReposURL() {
            return reposURL;
        }

        public Builder setReposURL(String reposURL) {
            this.reposURL = reposURL;
            return this;
        }



    }

    public String getGhPageURL() {
        return ghPageURL;
    }

    public String getBlogURL() {
        return blogURL;
    }

    public Iterable<GithubRepository> getOwnedRepos() {
        return ownedRepos;
    }

    public Map<String, Long> getLanguageAggr() {
        return languageAggr;
    }

    public String getGhUserName() {
        return ghUserName;
    }

    private GithubUserResume(Builder builder) {
        this.ghUserName = builder.ghUserName;
        this.ghPageURL = builder.ghPageURL;
        this.blogURL = builder.blogURL;
        this.languageAggr = builder.languageAggr;
        this.ownedRepos = builder.ownedRepos;
    }
    @Override
    public String toString() {
        return "GithubUserResume{" +
                "userName='" + ghUserName + '\'' +
                ", ghPageURL='" + ghPageURL + '\'' +
                ", blogURL='" + blogURL + '\'' +
                ", ownedRepos=" + ownedRepos +
                ", languageAggr=" + languageAggr +
                '}';
    }

}