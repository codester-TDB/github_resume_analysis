package githubresume.service;

import org.springframework.http.HttpMethod;

import java.util.HashMap;

public interface RestService<T> {
    public T getDetails(String url, HttpMethod httpMethod, HashMap<String, Object> params, Class<T> resultType);
}
