package githubresume.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.util.HashMap;

@Service
public class VanillaRestServiceImpl<T extends Serializable> implements RestService<T> {

    /*
    RestTemplate objs are immutable until unless we change their
    Http request converters. So we need to ensure we are not calling
    its setMessageConverters(). Making it private for some guard.
     */
    private static RestTemplate restTemplate;

    public VanillaRestServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        // By default we are sin
        this.restTemplate = restTemplateBuilder.errorHandler(
                new RestClientErrorResponseHandler()).build();
    }

    public <T extends ResponseErrorHandler> VanillaRestServiceImpl(
            RestTemplateBuilder restTemplateBuilder, T responseErrorHandler) {
        this.restTemplate = restTemplateBuilder.errorHandler(
                responseErrorHandler).build();
    }

    public VanillaRestServiceImpl(){}

    @Override
    public T getDetails(String url, HttpMethod httpMethod, HashMap<String, Object> params, Class<T> resultType) {
        return restTemplate.getForObject(url, resultType);
    }


}
