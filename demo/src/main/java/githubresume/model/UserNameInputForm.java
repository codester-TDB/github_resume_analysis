package githubresume.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotEmpty;

public class UserNameInputForm {
    @NotEmpty
    private String githubUserName;

    public String getGithubUserName() {
        return this.githubUserName;
    }

    public void setGithubUserName(String name) {
        this.githubUserName = name;
    }
}
