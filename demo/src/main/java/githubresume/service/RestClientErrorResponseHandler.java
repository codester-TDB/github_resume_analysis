package githubresume.service;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

import java.io.IOException;

@Component
public class RestClientErrorResponseHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
            return (
                    response.getStatusCode().series() == CLIENT_ERROR
                            || response.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().is4xxClientError()) {
            if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "UserName not found in GitHub."
                );
            }
            throw new ResponseStatusException(
                    response.getStatusCode(), "Not able access the resource with present setup or privileges."
            );
        } else if (response.getStatusCode().is5xxServerError()) {
            throw new ResponseStatusException(
                    response.getStatusCode(), "GitHub service is not able to process the request."
            );

        }

    }
}
